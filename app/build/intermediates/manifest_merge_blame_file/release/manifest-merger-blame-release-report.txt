1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.walton.productregistration"
4    android:versionCode="1"
5    android:versionName="1.0.3" >
6
7    <uses-sdk
8        android:minSdkVersion="23"
8-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml
9        android:targetSdkVersion="28" />
9-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
11-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:8:5-81
11-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:8:22-78
12    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
12-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:9:5-80
12-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:9:22-77
13    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
13-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:10:5-79
13-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:10:22-76
14    <uses-permission android:name="android.permission.INTERNET" />
14-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:11:5-83
14-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:11:22-64
15    <uses-permission android:name="android.permission.READ_PHONE_NUMBERS" />
15-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:12:5-77
15-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:12:22-74
16    <uses-permission android:name="android.permission.CAMERA" />
16-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:13:5-65
16-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:13:22-62
17    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
17-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:14:5-75
17-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:14:22-72
18    <uses-permission android:name="android.permission.SEND_SMS" />
18-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:15:5-83
18-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:15:22-64
19
20    <application
20-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:17:5-62:19
21        android:name="com.utils.UserDialogForLoad$AppController"
21-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:18:9-65
22        android:allowBackup="true"
22-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:19:9-35
23        android:appComponentFactory="whateverString"
23-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:20:9-53
24        android:icon="@drawable/marcel_logo"
24-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:21:9-45
25        android:label="@string/app_name"
25-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:22:9-41
26        android:theme="@style/AppTheme"
26-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:23:9-40
27        android:usesCleartextTraffic="true" >
27-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:24:9-44
28        <activity
28-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:27:9-29:63
29            android:name="com.walton.productregistration.SelectActivity"
29-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:28:13-43
30            android:theme="@android:style/Theme.NoTitleBar" />
30-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:29:13-60
31        <activity
31-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:30:9-34:63
32            android:name="com.walton.productregistration.MainActivity"
32-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:31:13-41
33            android:label="@string/app_name"
33-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:32:13-45
34            android:screenOrientation="portrait"
34-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:33:13-49
35            android:theme="@android:style/Theme.NoTitleBar" />
35-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:34:13-60
36        <activity
36-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:35:9-38:57
37            android:name="com.walton.productregistration.CollectionEntryActivity"
37-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:36:13-52
38            android:theme="@android:style/Theme.NoTitleBar"
38-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:37:13-60
39            android:windowSoftInputMode="stateHidden" />
39-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:38:13-54
40        <activity
40-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:39:9-41:48
41            android:name="com.walton.productregistration.ScannedBarcodeActivity"
41-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:40:13-51
42            android:theme="@style/AppTheme1" />
42-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:41:13-45
43        <activity
43-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:42:9-51:20
44            android:name="com.walton.productregistration.LoginActivity"
44-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:43:13-42
45            android:theme="@android:style/Theme.NoTitleBar"
45-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:44:13-60
46            android:windowSoftInputMode="stateHidden" >
46-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:45:13-54
47            <intent-filter>
47-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:46:13-50:29
48                <action android:name="android.intent.action.MAIN" />
48-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:47:17-69
48-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:47:25-66
49
50                <category android:name="android.intent.category.LAUNCHER" />
50-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:49:17-77
50-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:49:27-74
51            </intent-filter>
52        </activity>
53
54        <provider
54-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:53:9-61:20
55            android:name="androidx.core.content.FileProvider"
55-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:54:13-62
56            android:authorities="com.walton.productregistration.fileprovider"
56-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:55:13-78
57            android:exported="false"
57-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:56:13-37
58            android:grantUriPermissions="true" >
58-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:57:13-47
59            <meta-data
59-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:58:13-60:58
60                android:name="android.support.FILE_PROVIDER_PATHS"
60-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:59:17-67
61                android:resource="@xml/provider_paths" />
61-->F:\Android\WALTON PROJECT\waltonsms\app\src\main\AndroidManifest.xml:60:17-55
62        </provider>
63
64        <activity
64-->[com.karumi:dexter:5.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\a4abd16530d966f7b18d9f896bc33d5b\jetified-dexter-5.0.0\AndroidManifest.xml:27:9-29:72
65            android:name="com.karumi.dexter.DexterActivity"
65-->[com.karumi:dexter:5.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\a4abd16530d966f7b18d9f896bc33d5b\jetified-dexter-5.0.0\AndroidManifest.xml:28:13-60
66            android:theme="@style/Dexter.Internal.Theme.Transparent" />
66-->[com.karumi:dexter:5.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\a4abd16530d966f7b18d9f896bc33d5b\jetified-dexter-5.0.0\AndroidManifest.xml:29:13-69
67        <activity
67-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:23:9-27:75
68            android:name="com.google.android.gms.auth.api.signin.internal.SignInHubActivity"
68-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:24:13-93
69            android:excludeFromRecents="true"
69-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:25:13-46
70            android:exported="false"
70-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:26:13-37
71            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
71-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:27:13-72
72        <!--
73            Service handling Google Sign-In user revocation. For apps that do not integrate with
74            Google Sign-In, this service will never be started.
75        -->
76        <service
76-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:33:9-36:110
77            android:name="com.google.android.gms.auth.api.signin.RevocationBoundService"
77-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:34:13-89
78            android:exported="true"
78-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:35:13-36
79            android:permission="com.google.android.gms.auth.api.signin.permission.REVOCATION_NOTIFICATION" />
79-->[com.google.android.gms:play-services-auth:16.0.1] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\0d372dc0df938af8b1811fd3551e1578\jetified-play-services-auth-16.0.1\AndroidManifest.xml:36:13-107
80
81        <activity
81-->[com.google.android.gms:play-services-base:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\77c2fc448e2bd0738fd67dce062b1efd\play-services-base-17.0.0\AndroidManifest.xml:23:9-26:75
82            android:name="com.google.android.gms.common.api.GoogleApiActivity"
82-->[com.google.android.gms:play-services-base:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\77c2fc448e2bd0738fd67dce062b1efd\play-services-base-17.0.0\AndroidManifest.xml:24:13-79
83            android:exported="false"
83-->[com.google.android.gms:play-services-base:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\77c2fc448e2bd0738fd67dce062b1efd\play-services-base-17.0.0\AndroidManifest.xml:25:13-37
84            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
84-->[com.google.android.gms:play-services-base:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\77c2fc448e2bd0738fd67dce062b1efd\play-services-base-17.0.0\AndroidManifest.xml:26:13-72
85
86        <meta-data
86-->[com.google.android.gms:play-services-basement:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\bfab97caa09a1c39181f59d97551df5a\jetified-play-services-basement-17.0.0\AndroidManifest.xml:23:9-25:69
87            android:name="com.google.android.gms.version"
87-->[com.google.android.gms:play-services-basement:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\bfab97caa09a1c39181f59d97551df5a\jetified-play-services-basement-17.0.0\AndroidManifest.xml:24:13-58
88            android:value="@integer/google_play_services_version" />
88-->[com.google.android.gms:play-services-basement:17.0.0] C:\Users\Mithun\.gradle\caches\transforms-2\files-2.1\bfab97caa09a1c39181f59d97551df5a\jetified-play-services-basement-17.0.0\AndroidManifest.xml:25:13-66
89    </application>
90
91</manifest>
