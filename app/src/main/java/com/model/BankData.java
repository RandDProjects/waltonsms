package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class BankData implements Parcelable {

    List<BankAccountInfo> bankAccountInfoList = new ArrayList<>();

    protected BankData(Parcel in) {
        bankAccountInfoList = in.createTypedArrayList(BankAccountInfo.CREATOR);
    }

    public static final Creator<BankData> CREATOR = new Creator<BankData>() {
        @Override
        public BankData createFromParcel(Parcel in) {
            return new BankData(in);
        }

        @Override
        public BankData[] newArray(int size) {
            return new BankData[size];
        }
    };

    public List<BankAccountInfo> getBankAccountInfoList() {
        return bankAccountInfoList;
    }

    public void setBankAccountInfoList(List<BankAccountInfo> bankAccountInfoList) {
        this.bankAccountInfoList = bankAccountInfoList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(bankAccountInfoList);
    }
}
