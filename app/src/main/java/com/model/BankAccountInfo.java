package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankAccountInfo implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("branchName")
    @Expose
    private String branchName;
    @SerializedName("ou")
    @Expose
    private String ou;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("receiptMethodId")
    @Expose
    private Integer receiptMethodId;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("bankAccountType")
    @Expose
    private String bankAccountType;
    @SerializedName("remitBankAcctUseId")
    @Expose
    private Integer remitBankAcctUseId;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("lastUpdateDate")
    @Expose
    private String lastUpdateDate;
    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;
    public final static Parcelable.Creator<BankAccountInfo> CREATOR = new Creator<BankAccountInfo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BankAccountInfo createFromParcel(Parcel in) {
            return new BankAccountInfo(in);
        }

        public BankAccountInfo[] newArray(int size) {
            return (new BankAccountInfo[size]);
        }

    }
            ;

    protected BankAccountInfo(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.branchName = ((String) in.readValue((String.class.getClassLoader())));
        this.ou = ((String) in.readValue((String.class.getClassLoader())));
        this.organizationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.receiptMethodId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.companyName = ((String) in.readValue((String.class.getClassLoader())));
        this.bankAccountType = ((String) in.readValue((String.class.getClassLoader())));
        this.remitBankAcctUseId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastUpdateDate = ((String) in.readValue((String.class.getClassLoader())));
        this.bankAccountName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BankAccountInfo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getReceiptMethodId() {
        return receiptMethodId;
    }

    public void setReceiptMethodId(Integer receiptMethodId) {
        this.receiptMethodId = receiptMethodId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(String bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public Integer getRemitBankAcctUseId() {
        return remitBankAcctUseId;
    }

    public void setRemitBankAcctUseId(Integer remitBankAcctUseId) {
        this.remitBankAcctUseId = remitBankAcctUseId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(createdDate);
        dest.writeValue(branchName);
        dest.writeValue(ou);
        dest.writeValue(organizationId);
        dest.writeValue(receiptMethodId);
        dest.writeValue(companyName);
        dest.writeValue(bankAccountType);
        dest.writeValue(remitBankAcctUseId);
        dest.writeValue(bankName);
        dest.writeValue(lastUpdateDate);
        dest.writeValue(bankAccountName);
    }

    public int describeContents() {
        return 0;
    }

}
