package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepositorBank implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("flexValueMeaning")
    @Expose
    private String flexValueMeaning;
    @SerializedName("description")
    @Expose
    private String description;
    public final static Parcelable.Creator<BankAccountInfo> CREATOR = new Creator<BankAccountInfo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BankAccountInfo createFromParcel(Parcel in) {
            return new BankAccountInfo(in);
        }

        public BankAccountInfo[] newArray(int size) {
            return (new BankAccountInfo[size]);
        }

    }
            ;

    protected DepositorBank(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.flexValueMeaning = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
    }

    public DepositorBank() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFlexValueMeaning() {
        return flexValueMeaning;
    }

    public void setFlexValueMeaning(String flexValueMeaning) {
        this.flexValueMeaning = flexValueMeaning;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(createdDate);
        dest.writeValue(flexValueMeaning);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

}
