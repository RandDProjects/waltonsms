package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppsCollection implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("ou")
    @Expose
    private String ou;
    @SerializedName("depositDate")
    @Expose
    private String depositDate;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("bankAccount")
    @Expose
    private String bankAccount;
    @SerializedName("depositorBank")
    @Expose
    private String depositorBank;
    @SerializedName("entryDate")
    @Expose
    private String entryDate;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("depositorBankBranch")
    @Expose
    private String depositorBankBranch;
    @SerializedName("customerCode")
    @Expose
    private String customerCode;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    public final static Parcelable.Creator<AppsCollection> CREATOR = new Creator<AppsCollection>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AppsCollection createFromParcel(Parcel in) {
            return new AppsCollection(in);
        }

        public AppsCollection[] newArray(int size) {
            return (new AppsCollection[size]);
        }

    }
            ;

    protected AppsCollection(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.ou = ((String) in.readValue((String.class.getClassLoader())));
        this.depositDate = ((String) in.readValue((String.class.getClassLoader())));
        this.customerName = ((String) in.readValue((String.class.getClassLoader())));
        this.bankAccount = ((String) in.readValue((String.class.getClassLoader())));
        this.depositorBank = ((String) in.readValue((String.class.getClassLoader())));
        this.entryDate = ((String) in.readValue((String.class.getClassLoader())));
        this.remarks = ((String) in.readValue((String.class.getClassLoader())));
        this.createdBy = ((String) in.readValue((String.class.getClassLoader())));
        this.depositorBankBranch = ((String) in.readValue((String.class.getClassLoader())));
        this.customerCode = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Double) in.readValue((Double.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public AppsCollection() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getDepositorBank() {
        return depositorBank;
    }

    public void setDepositorBank(String depositorBank) {
        this.depositorBank = depositorBank;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDepositorBankBranch() {
        return depositorBankBranch;
    }

    public void setDepositorBankBranch(String depositorBankBranch) {
        this.depositorBankBranch = depositorBankBranch;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(createdDate);
        dest.writeValue(ou);
        dest.writeValue(depositDate);
        dest.writeValue(customerName);
        dest.writeValue(bankAccount);
        dest.writeValue(depositorBank);
        dest.writeValue(entryDate);
        dest.writeValue(remarks);
        dest.writeValue(createdBy);
        dest.writeValue(depositorBankBranch);
        dest.writeValue(customerCode);
        dest.writeValue(amount);
        dest.writeValue(bankName);
    }

    public int describeContents() {
        return 0;
    }

}
