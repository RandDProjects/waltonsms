package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerInfo implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("partyId")
    @Expose
    private Integer partyId;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("salesChannelCode")
    @Expose
    private String salesChannelCode;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    public final static Parcelable.Creator<CustomerInfo> CREATOR = new Creator<CustomerInfo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CustomerInfo createFromParcel(Parcel in) {
            return new CustomerInfo(in);
        }

        public CustomerInfo[] newArray(int size) {
            return (new CustomerInfo[size]);
        }

    }
            ;

    protected CustomerInfo(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.partyId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.salesChannelCode = ((String) in.readValue((String.class.getClassLoader())));
        this.customerName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CustomerInfo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPartyId() {
        return partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getSalesChannelCode() {
        return salesChannelCode;
    }

    public void setSalesChannelCode(String salesChannelCode) {
        this.salesChannelCode = salesChannelCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(partyId);
        dest.writeValue(createdDate);
        dest.writeValue(salesChannelCode);
        dest.writeValue(customerName);
    }

    public int describeContents() {
        return 0;
    }

}