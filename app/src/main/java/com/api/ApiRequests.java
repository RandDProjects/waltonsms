package com.api;


import com.google.gson.JsonObject;
import com.model.AppsCollection;
import com.model.BankAccountInfo;
import com.model.DepositorBank;
import com.model.Login;

import java.util.List;

import com.model.CustomerInfo;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiRequests {


    /************** Authentication******************/
    public final String URL = "http://192.168.1.100:8080/";

    @FormUrlEncoded
    @POST("smsinbox/api/SmsInbox/")
    Single<String> sendData(@Field("Sender") String sender, @Field("Message") String message);


    @Headers("Content-Type: application/json")
    @POST("revAPI/api/login")
    Call<Login> login(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("revAPI/appsCollection")
    Call<AppsCollection> appcollecton(@Header("Authorization") String auth, @Body JsonObject jsonObject);

    /*
        @Header("Authorization:  Bearer eyJhbGciOiJIUzI1NiJ9.eyJwcmluY2lwYWwiOiJINHNJQUFBQUFBQUFBSlZTdlVcL2JRQlJcL1RoTlJnVlNnRWtnZFlBRTI1RWgwek1SbjFjb05xR2tXa0VBWCsrRWVuT1wvTTNSbVNwY3BFaHd5cGFKRXFkZTNJZjlJdVwvUU5RTzNSbDd0cDNodURBZ25xVFwvZTduMzlmenhSVlVqSWJuc1daY0dEOFZXY3lsYjFMTlpXd3d6RFMzSFQ4enFDTzBPZUpGRG16U0JLNlBWd0l2Z0JLUExEd05EdGd4cXdvbTQrcG02d0JEVzJ0cldGSTZ2bUhjMXl6QkU2VVBcL1Z2dVVHbThJMUJRZTk5S01MSU5reXdNVlNadFhjbjFkc28xUnRzd1Vjd0NGUjY2MFZSSU55Z3RaOElNUTBkUXNwYkFLSUF4bHRsM2lsUTVHZ3ZqMTJZenkwVzFnYllXd09PVUdVUHU3aVZwV0dmZDNUdWJraEljd1hzb3QxT1BEblczNEtDKzRcL0ZYbFJDVW1pdHA1cHN5VVJIZjUwNmMrTHN6WnpcLzdYN3ZORWdCMXN2andOOFg4MlFwMHYrXC8rbmMyTDlrSUwwMFBXQzFpdG5aS2J5WUw1clVhbmZQbGw2OVA1MVllZFI2VHNFQnZcL3Y0XC81NVp2bU9xc3FTWmxtVmczdGlHaFB5dTZaeUZjZUpoOXNvZU0zZUpJS3BEOUtXb3h1SlFwaWlsdldTZ3o2dGpENlpqTlkzMXRlZVwvMnk3bDRyTEVxNEpOVW5lV2kzTFQ5UXRLdmVuNDhcLytuT1wvaU9FVlZJNlp5SkE2bnloQTlTeHBvVDY5T0o4WitcL3k3bHljWVwvTTNcL0FNaDBvOTRSQXdBQSIsInN1YiI6ImFkbWluIiwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJpc3MiOiJTcHJpbmcgU2VjdXJpdHkgUkVTVCBHcmFpbHMgUGx1Z2luIiwiZXhwIjoxNTgxMTQzMDE1LCJpYXQiOjE1ODExMzk0MTV9.jT-MmPodLp65pnfyIl_O82uE2X8PJ1f3T5C3MSZvuU0")
    */
    @Headers("Content-Type: application/json")
    @GET("revAPI/ebsBankAcInfo")
    Call<List<BankAccountInfo>> getCompanyBank(@Header("Authorization") String auth);

    @Headers("Content-Type: application/json")
    @GET("revAPI/ebsBankAcInfo")
    Call<List<BankAccountInfo>> getSpinnerData(@Header("Authorization") String auth, @Query("organizationId")int id);

    @Headers("Content-Type: application/json")
    @GET("revAPI/depositorBankInfo/")
    Call<List<DepositorBank>> getDepositorBank(@Header("Authorization") String auth);

    @Headers("Content-Type: application/json")
    @GET("revAPI/ebsPartyInfo/")
    Call<List<CustomerInfo>> getCustomerInfo(@Header("Authorization") String auth);
}


