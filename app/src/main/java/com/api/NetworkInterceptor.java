package com.api;

import android.util.Log;


import com.utils.SharedPreference;
import com.utils.UserDialogForLoad;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by newton on 7/6/17.
 */

public class NetworkInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        NetworkModule networkModule = new NetworkModule(UserDialogForLoad.AppController.getInstance());
        if (networkModule.isConnected()) {

            return chain.proceed(getRequest(chain.request()));
        } else
            throw new NoNetworkException();
    }

    private Request getRequest(Request request){
        HttpUrl rootUrl = request.url();
        HttpUrl callUrl = rootUrl.newBuilder()
               // .addQueryParameter("Bearer ", SharedPreference.getStringValue(UserDialogForLoad.AppController.getInstance(),SharedPreference.AUTHRIZATION))
                .build();
        Log.e("call url", String.valueOf(callUrl));
        return request.newBuilder()
                .url(callUrl)
               // .addHeader("Content-type", "application/x-www-form-urlencoded")
               // .addHeader("Content-type", "application/json")
               // .addHeader("Authorization", "Bearer :")
                .build();

    }

}
