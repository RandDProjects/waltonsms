package com.api;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.model.AppsCollection;
import com.model.BankAccountInfo;
import com.model.BankData;
import com.model.Login;
import com.utils.ErrorData;
import com.utils.SharedPreference;
import com.utils.UserDialogForLoad;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by newton on 7/9/17.
 */

public class ApiModule {
    private ApiRequests mApiRequests;
    private ResponseListener mResponseListener;
    private Context mContext;


    public ApiModule(Context context, ResponseListener listener) {
        mApiRequests = SetReadyApiRequest.getInstance().getRetrofit(SetReadyApiRequest.getInstance().getOkHttpClient()).create(ApiRequests.class);
        mResponseListener = listener;
        mContext = context;
    }


    public void sendData(String sender, String message) {
        mApiRequests.sendData(sender, message)
                .delay(600, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        getExceptions(throwable);
                    }

                    @Override
                    public void onSuccess(String otp) {
                        mResponseListener.getResponse(otp);
                    }
                });
    }
 /*   public void login(JsonObject jsobj) {
        Log.e("sign", String.valueOf(jsobj));
        mApiRequests.login(jsobj)
                .delay(600, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Login>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        getExceptions(throwable);
                    }

                    @Override
                    public void onSuccess(Login otp) {
                        mResponseListener.getResponse(otp);
                    }
                });
    }
*/
 /*   public void operatinUnit(String jsobj) {
        Log.e("sign", String.valueOf(jsobj));
        mApiRequests.getCompanyBank(jsobj)
                .delay(600, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<BankAccountInfo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        getExceptions(throwable);
                    }

                    @Override
                    public void onSuccess(List<BankAccountInfo> otp) {
                        mResponseListener.getResponse(otp);
                    }
                });
    }*/
/*    public void depositorBank(String jsobj) {
        Log.e("sign", String.valueOf(jsobj));
        mApiRequests.getDepositorBank(jsobj)
                .delay(600, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<BankData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        getExceptions(throwable);
                    }

                    @Override
                    public void onSuccess(BankData otp) {
                        mResponseListener.getResponse(otp);
                    }
                });
    }*/
    private void getExceptions(Throwable throwable) {
        if (throwable instanceof NoNetworkException) {
            UserDialogForLoad.AppController.getInstance().showToastShort("No connectivity");
            mResponseListener.getError("No connectivity");
        } else if (throwable instanceof HttpException) {
            int code = ((HttpException) throwable).code();

            Log.e("error code", String.valueOf(code));
            System.out.println(" err code == " + code + "/" +
                    ((HttpException) throwable).response().isSuccessful() + "/" +
                    ((HttpException) throwable).response().body() + "/" +
                    ((HttpException) throwable).response().message() + "/" +
                    ((HttpException) throwable).response().errorBody().byteStream().toString());

            switch (code) {
                case 500:
                    mResponseListener.getError(((HttpException) throwable).message());
                    break;
                case 501:
                    mResponseListener.getError(((HttpException) throwable).message());
                    break;
                case 401:
                    Object response = getError(((HttpException) throwable).response());
                    if (response instanceof ErrorData) {
                        ErrorData mErrorData = (ErrorData) response;
                        System.out.println("error 401 : " + mErrorData.getErrorData());

                        mResponseListener.getError(mErrorData.getErrorData());
                    }
                  //  SharedPreference.setBooleanValue(mContext, SharedPreference.LOG_IN_STATUS, false);
                 //   SharedPreference.setStringValue(mContext, SharedPreference.API_TOKEN, null);
//                    Intent intent = new Intent(mContext, SignupOrLoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    mContext.startActivity(intent);

//                        mResponseListener.getError((String) response);

                    break;
                case 400:
                    response = getError(((HttpException) throwable).response());
                    Log.e("error response else", new Gson().toJson(response));
                 /*   if (response instanceof ErrorData) {
                        try {
                            Log.e("error response", new Gson().toJson(response));
                            ErrorData mErrorData = (ErrorData) response;
                            mResponseListener.getError(mErrorData.getErrorData());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("error response else", new Gson().toJson(response));
                        mResponseListener.getError((String) response);
                    }*/
                    break;
                case 403:
                    mResponseListener.getError(((HttpException) throwable).message());
                    break;
                case 404:
                    mResponseListener.getError(((HttpException) throwable).message());
                    break;
                default:
                    response = getError(((HttpException) throwable).response());
                    if (response instanceof ErrorData) {
                        try {
                            Log.e("default 1 response ", new Gson().toJson(response));
                            ErrorData mErrorData = (ErrorData) response;
                            mResponseListener.getError(mErrorData.getErrorData());


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("default 1 message ", new Gson().toJson(response));
                        mResponseListener.getError((String) response);
                    }

                    Log.e("default 1", "" + ((HttpException) throwable).message());
                    mResponseListener.getError(((HttpException) throwable).message());
                    break;
            }
        } else if (throwable instanceof SocketTimeoutException) {
            mResponseListener.getError("Request time out");
        } else {
            Log.e("default 2", "" + throwable.getMessage());
            mResponseListener.getError("" + throwable.getMessage());
        }
    }

    private Object getError(Response errorData) {

        try {
            InputStream is = errorData.errorBody().byteStream();
            if (is != null) {
                BufferedReader r = new BufferedReader(new InputStreamReader(is));
                StringBuilder errorResult = new StringBuilder();
                String line;
                try {
                    while ((line = r.readLine()) != null) {
                        errorResult.append(line).append("\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                return gson.fromJson(errorResult.toString(), ErrorData.class);


            } else return "Try again later!";
        } catch (Exception e) {
            return "" + errorData.message();
        }

    }
}
