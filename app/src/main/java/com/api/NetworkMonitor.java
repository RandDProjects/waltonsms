package com.api;

/**
 * Created by newton on 7/6/17.
 */

public interface NetworkMonitor {
    boolean isConnected();
}
