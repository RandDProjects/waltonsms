package com.example.barcode.assyn;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;



import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

@SuppressWarnings("unused")
public class AsyncTaskProgressDialog implements Runnable {

	ProgressDialog pd;
	RequestListener requestlistner;
	AsyncTaskProgressDialog(Context context, RequestListener requestlistner )
	{
		requestlistner = requestlistner;
		pd = ProgressDialog.show(context, "Working..",
				"Calculating Pi", true, false);
		Thread t = new Thread();
		t.start();
	}
	


	public void run() {
		// calculating PI
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// calculation completed
		handler.sendEmptyMessage(0);

	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			//requestlistner.onComplete(response, state)setText("Value of PI: " + Math.PI);
		}
	};
	

	/**
	 * Callback interface for API requests.
	 *
	 * Each method includes a 'state' parameter that identifies the calling
	 * request. It will be set to the value passed when originally calling the
	 * request method, or null if none was passed.
	 */
	public static interface RequestListener {

	    /**
	     * Called when a request completes with the given response.
	     *
	     * Executed by a background thread: do not update the UI in this method.
	     */
	    public void onComplete(String response, Object state);

	    /**
	     * Called when a request has a network or request error.
	     *
	     * Executed by a background thread: do not update the UI in this method.
	     */
	    public void onIOException(IOException e, Object state);

	    /**
	     * Called when a request fails because the requested resource is
	     * invalid or does not exist.
	     *
	     * Executed by a background thread: do not update the UI in this method.
	     */
	    public void onFileNotFoundException(FileNotFoundException e,
	                                        Object state);

	    /**
	     * Called if an invalid graph path is provided (which may result in a
	     * malformed URL).
	     *
	     * Executed by a background thread: do not update the UI in this method.
	     */
	    public void onMalformedURLException(MalformedURLException e,
	                                        Object state);

	    /**
	     * Called when the server-side Facebook method fails.
	     *
	     * Executed by a background thread: do not update the UI in this method.
	     */
	  //  public void onFacebookError(FacebookError e, Object state);
}


}