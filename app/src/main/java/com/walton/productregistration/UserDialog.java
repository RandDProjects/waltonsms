package com.walton.productregistration;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class UserDialog {
    public static void showUserAlert(final Context context, String message,String title) {


        final Dialog dialog = new Dialog(context);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customuser_popoup_dialog);
       /* AlertDialog.Builder alert_builder = new AlertDialog.Builder(context);
        alert_builder.setView(view);
        final AlertDialog alert = alert_builder.create();*/

        TextView tvTitle = dialog.findViewById(R.id.idDialogHeader);
        tvTitle.setText(title);
        TextView tvMessage = dialog.findViewById(R.id.idAlertMessage);
        tvMessage.setText(message);

        Button btnConfirm = dialog.findViewById(R.id.idConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
