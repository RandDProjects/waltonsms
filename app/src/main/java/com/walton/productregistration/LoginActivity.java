package com.walton.productregistration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.api.ApiRequests;
import com.google.gson.JsonObject;
import com.model.Login;
import com.utils.SharedPreference;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends Activity {
    TextView tvLogin;
    EditText etUserName, etPassword;
    Context mContext;
    String username, password;
    ProgressDialog progressDialog;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        mContext = this;
        progressDialog = new ProgressDialog(mContext);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        tvLogin = findViewById(R.id.tvlogin);
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (etUserName.getText().toString() != null && etPassword.getText().toString() != null) {
                   /* username = etUserName.getText().toString();
                    password = etPassword.getText().toString();*/
                   username="admin";
                   password="123456";

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("username", username);
                    jsonObject.addProperty("password", password);
                    Log.e("json format", String.valueOf(jsonObject));

                    loginData(jsonObject);
                } else {
                    Toast.makeText(mContext, "Please Enter Input Feilds", Toast.LENGTH_SHORT).show();
                }


                //startActivity(new Intent(mContext, CollectionEntryActivity.class));
            }
        });

    }

    private void loginData(JsonObject jsonObject) {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(ApiRequests.URL).
                addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<Login> callSpecificaiotn = apiInterface.login(jsonObject);
        callSpecificaiotn.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Login login = response.body();
                String AUTH = login.getAccessToken();
                String username = login.getUsername();
                SharedPreference.setStringValue(mContext, SharedPreference.USER_ID, username);
                Log.e("auth", AUTH);
                SharedPreference.setStringValue(mContext, SharedPreference.AUTHRIZATION, AUTH);
                progressDialog.dismiss();
                if (username.length()>0){
                    startActivity(new Intent(mContext, SelectActivity.class));
                }


            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.e("failur", "failur submit feedback");
                progressDialog.dismiss();
                Log.e("error", t.getMessage());

            }
        });
    }

    @Override
    public void onBackPressed() {
        closeAlert();

    }

    private void closeAlert() {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_custom);
        Button btnYes = dialog.findViewById(R.id.btnYes);
        Button btnNo = dialog.findViewById(R.id.btnNo);

        // if button is clicked, close the custom dialog
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });

        dialog.show();
    }
}

