package com.walton.productregistration;

import java.text.DateFormatSymbols;
import java.util.List;

import com.api.ApiModule;
import com.api.ResponseListener;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.utils.UserDialogForLoad;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.commons.lang3.StringUtils;

public class MainActivity extends Activity implements android.view.View.OnClickListener, ResponseListener {


    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int PERMISSION_REQUEST_CODE_PHONE = 200;

    private Button btnGo, btnClear, btnBarcodeScanner;
    private EditText txtProductSerial, txtCustomerName, txtCustomerMobileNo, txtCustomerAddress;
    Context context;
    String scanFormatSerial, ProductSerialID, CompressorSerialID, currentDate, VersionID, customerName, customerMobile, customerAddress, finalMessage;
    ProgressBar progressBar;
    boolean isProduct = false, isCompresor = false;
    TextView txt_Developer_Tittle;
    String simType, currentLanguage;
    Dialog dialog = null;
    String deviceOwnerNumber = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanar_screen);
        context = MainActivity.this;
        requestStoragePermission();
        btnGo = (Button) findViewById(R.id.btnGo);
        btnClear = (Button) findViewById(R.id.btnClear);
        txtProductSerial = (EditText) findViewById(R.id.txtProductTittleNo);
        txtCustomerName = (EditText) findViewById(R.id.txtCustomerName);
        txtCustomerMobileNo = (EditText) findViewById(R.id.txtCustomerMobileNo);
        txtCustomerAddress = (EditText) findViewById(R.id.txtCustomerAddress);
        btnBarcodeScanner = findViewById(R.id.btnBarCodeScanner);

        txt_Developer_Tittle = (TextView) findViewById(R.id.txt_Developer_Tittle);
        txt_Developer_Tittle.setText("Developed By @Walton  Mobile Software R & D.");

        txtProductSerial.setOnClickListener((OnClickListener) this);
        btnGo.setOnClickListener((OnClickListener) this);
        btnClear.setOnClickListener((OnClickListener) this);
        btnBarcodeScanner.setOnClickListener(this);
    }

    public void onClick(View v) {
        //respond to clicks
        if (v.getId() == R.id.btnGo) {
            ProductSerialID = txtProductSerial.getText().toString().trim();
            customerName = txtCustomerName.getText().toString().trim();
            customerMobile = txtCustomerMobileNo.getText().toString().trim();
            customerAddress = txtCustomerAddress.getText().toString().trim();
            if (inputValidation()) {
                if (ProductSerialID.length() > 0 && customerName.length() > 0 && customerMobile.length()== 11 && customerAddress.length() > 0) {
                    if (!ProductSerialID.equalsIgnoreCase("null")) {
                        MoreHelp(ProductSerialID, customerName, customerMobile, customerAddress);
                    } else {
                        //  Toast.makeText(context, "INVALID CODE ????", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // Toast.makeText(context, "Input Data Nil", Toast.LENGTH_LONG).show();
                }
            } else {
                // Toast.makeText(context, "phone number is not valid", Toast.LENGTH_SHORT).show();
            }

        }

        if (v.getId() == R.id.btnBarCodeScanner) {
            Intent intent = new Intent(context, ScannedBarcodeActivity.class);
            startActivityForResult(intent, 1001);

        }
        if (v.getId() == R.id.btnClear) {
            txtProductSerial.setText("");
            txtCustomerName.setText("");
            txtCustomerMobileNo.setText("");
            txtCustomerAddress.setText("");
        }


    }

    @TargetApi(Build.VERSION_CODES.N)
    private boolean inputValidation() {
        if (txtProductSerial.getText() == null || txtProductSerial.getText().toString().isEmpty()) {
            UserDialog.showUserAlert(context, getString(R.string.barcode_scan_serial_no_alert),"প্রোডাক্ট রেজিস্ট্রেশান");
            return false;
        } else if (txtCustomerName.getText().toString().trim().isEmpty()) {
            UserDialog.showUserAlert(context, getString(R.string.customer_name_alert),"প্রোডাক্ট রেজিস্ট্রেশান");
            return false;
        } else if (txtCustomerMobileNo.getText().toString().trim().isEmpty()) {
            UserDialog.showUserAlert(context, getString(R.string.mobile_number_alert),"প্রোডাক্ট রেজিস্ট্রেশান");
            return false;
        } else if (txtCustomerMobileNo.getText().toString().length() != 11) {
            UserDialog.showUserAlert(context, getString(R.string.mobile_number_alert_num),"প্রোডাক্ট রেজিস্ট্রেশান");
            return false;

        } else if (!StringUtils.startsWithAny(txtCustomerMobileNo.getText().toString(), new String[]{"016", "018", "017", "015", "014", "019", "013"})) {
            UserDialog.showUserAlert(context, "There is no " + txtCustomerMobileNo.getText().toString().substring(0, Math.min(txtCustomerMobileNo.getText().toString().length(), 3)) + " type Operator","প্রোডাক্ট রেজিস্ট্রেশান");
            return false;
        } else if (txtCustomerAddress.getText() == null || txtCustomerAddress.getText().toString().isEmpty()) {
            UserDialog.showUserAlert(context, getString(R.string.current_address_alert),"প্রোডাক্ট রেজিস্ট্রেশান");
            return false;
        }
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        if (requestCode == 1001) {
            String result;
            try {
                result = intent.getStringExtra("result");
                txtProductSerial.setText("" + result);
                Log.e("ggggggggggggg", result);
            } catch (Exception e) {
                result = "";
            }

        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }


    public void onClick(DialogInterface dialog, int which) {
        // TODO Auto-generated method stub
    }


    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

/*
    protected void sendSMS(String phoneNo, String msg) {


        try {

            String SENT = "sent";
            String DELIVERED = "delivered";

            Intent sentIntent = new Intent(SENT);
            */
    /*Create Pending Intents*//*

            PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Intent deliveryIntent = new Intent(DELIVERED);
            PendingIntent deliverPI = PendingIntent.getBroadcast(getApplicationContext(), 0, deliveryIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            */
    /* Register for SMS send action *//*

            registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String result = "";

                    switch (getResultCode()) {

                        case Activity.RESULT_OK:
                            result = "Transmission successful";
                            txtProductSerial.setText("");
                            txtCustomerName.setText("");
                            txtCustomerMobileNo.setText("");
                            txtCustomerAddress.setText("");
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            result = "Transmission failed";
                            if (simType.equalsIgnoreCase("grameen")) {
                                sendSMS("01686690014", finalMessage);

                            } else if (simType.equalsIgnoreCase("airtel")) {
                                sendSMS("01713449250", finalMessage);
                            }

                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            result = "SMS Getway Off";
                            if (simType.equalsIgnoreCase("grameenphone")) {
                                sendSMS("01686690014", finalMessage);

                            } else if (simType.equalsIgnoreCase("airtel")) {
                                sendSMS("01713449250", finalMessage);
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            result = "No PDU defined";
                            if (simType.equalsIgnoreCase("grameenphone")) {
                                sendSMS("01686690014", finalMessage);

                            } else if (simType.equalsIgnoreCase("airtel")) {
                                sendSMS("01713449250", finalMessage);
                            }
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            result = "No service";
                            if (simType.equalsIgnoreCase("grameenphone")) {
                                sendSMS("01686690014", finalMessage);

                            } else if (simType.equalsIgnoreCase("airtel")) {
                                sendSMS("01713449250", finalMessage);
                            }
                            break;
                        default:
                            if (simType.equalsIgnoreCase("grameenphone")) {
                                sendSMS("01686690014", finalMessage);

                            } else if (simType.equalsIgnoreCase("airtel")) {
                                sendSMS("01713449250", finalMessage);
                            }
                    }

                   // Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    UserDialog.showUserAlert(context, result + "");

                }

            }, new IntentFilter(SENT));
            */
    /* Register for Delivery event *//*

            registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Toast.makeText(getApplicationContext(), "Send Ok",
                            Toast.LENGTH_LONG).show();
                }

            }, new IntentFilter(DELIVERED));

            */
    /*Send SMS*//*

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.SEND_SMS)) {
                } else {
                    // permission is already granted
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 123);
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, msg, sentPI, deliverPI);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, sentPI, deliverPI);
                Toast.makeText(getApplicationContext(), "SMS sent.",
                        Toast.LENGTH_LONG).show();
            }


        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),
                    ex.getMessage().toString(), Toast.LENGTH_LONG)
                    .show();
            UserDialog.showUserAlert(context, ex + ""+" please check balance");

            ex.printStackTrace();
        }

    }
*/


    private void sendData(String sender, String message) {
        UserDialogForLoad.getInstanc().showProgressDialog(context);

        Log.e("owner number", sender + "  ====   " + message);
        ApiModule apiModule = new ApiModule(context, this);
        apiModule.sendData(sender, message);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("PLAYGROUND", "Permission has been granted");

            } else {
                Log.e("PLAYGROUND", "Permission has been denied or request cancelled");

            }
        } else if (requestCode == 124) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("PLAYGROUND", "PHONE Number Permission has been granted");

            } else {
                Log.e("PLAYGROUND", "Phone Number Permission has been denied or request cancelled");

            }
        } else if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use local drive .");
            } else {
                Log.e("storage", "Permission Denied, You cannot use local drive .");
            }
        } else if (requestCode == PERMISSION_REQUEST_CODE_PHONE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use local drive .");
            } else {
                Log.e("phone", "Permission Denied, You cannot use local drive .");
            }
        }

    }

    public void MoreHelp(final String serialkey, final String customerName, final String customerMobile, String customerPresentAddress) {

        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);

        TextView txtSerialKey = (TextView) dialog.findViewById(R.id.textSerial);
        TextView txtName = (TextView) dialog.findViewById(R.id.textWarrenty);
        TextView txtMobile = (TextView) dialog.findViewById(R.id.txtMobile);
        TextView txtAddress = (TextView) dialog.findViewById(R.id.txtAddress);

        txtSerialKey.setText(serialkey);
        txtName.setText(customerName);
        txtMobile.setText(customerMobile);
        txtAddress.setText(customerPresentAddress);
        finalMessage = "reg&&" + serialkey + "&&" + customerName + "&&" + customerMobile + "&&" + customerPresentAddress;

//		if(customerName.length()>0){
//			txtWarrenty.setText(customerName);
//		   finalMessage= "reg "+serialkey+" "+customerName;
//		}
//		else{
//			txtWarrenty.setText("N/A");
//		   finalMessage="reg "+serialkey;
//		}
        Button btYes = (Button) dialog.findViewById(R.id.dialogButtonOK);
        Button btNo = (Button) dialog.findViewById(R.id.dialogButtonNO);
        btYes.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                //sendSMS("01755611111",finalMessage);

                sendData(deviceOwnerNumber, finalMessage);

              /*  switch (simType) {
                    case "grameenphone":
                        simType = "grameenphone";
                        sendSMS("01713449250", finalMessage);
                        break;
                    case "banglalink":
                        simType = "banglalink";
                        sendSMS("01401478908", finalMessage);
                        break;
                    case "airtel":
                        Log.e("operator", "airtel");
                        simType = "airtel";
                        sendSMS("01686690014", finalMessage);
                        break;
                    case "robi":
                        simType = "robi";
                        sendSMS("01844167011", finalMessage);
                        break;
                    case "teletalk":
                        simType = "teletalk";
                        sendSMS("01713449250", finalMessage);
                        break;
                    default:
                        sendSMS("01713449250", finalMessage);
                        break;
                }*/
            }
        });

        btNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                txtProductSerial.setText("");
                txtCustomerName.setText("");
                txtCustomerMobileNo.setText("");
                txtCustomerAddress.setText("");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void getResponse(Object responseData) {
        UserDialogForLoad.getInstanc().stopProgressDialog();

        if (responseData instanceof String) {
            Log.e("response", String.valueOf(responseData));

            UserDialog.showUserAlert(context, responseData + "","প্রোডাক্ট রেজিস্ট্রেশান");
        } else {
            Log.e("data", String.valueOf(responseData));
        }
        dialog.dismiss();
    }

    @Override
    public void getError(String errMsg) {
        UserDialogForLoad.getInstanc().stopProgressDialog();
        Log.e("error babul", errMsg);
        dialog.dismiss();
        UserDialog.showUserAlert(context, errMsg + "","প্রোডাক্ট রেজিস্ট্রেশান");


    }

    private void requestStoragePermission() {
        Dexter.withActivity(this).withPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {

                }
                if (report.isAnyPermissionPermanentlyDenied()) {
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                }).onSameThread().check();
    }
}
