package com.walton.productregistration;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.api.ApiRequests;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.model.AppsCollection;
import com.model.BankAccountInfo;
import com.model.DepositorBank;
import com.utils.InterNetConnection;
import com.utils.SharedPreference;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import adapter.BankAccountName;
import adapter.BankInfoAdapter;
import adapter.CompanyBankNameApater;

import com.model.CustomerInfo;

import org.apache.commons.lang3.StringUtils;

import adapter.DepositorBankAdapter;
import adapter.MyCustomPagerAdapter;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class CollectionEntryActivity extends Activity {
    ImageView camera, gallery;
    Spinner sprOperatingUnit, spnrComapnyBankName, sprBankAccountName, spnrDepositorBankAccoount;
    EditText edtamount, edtCalender;
    Context mContext;
    private Calendar mcalendar;
    private EditText etDate;
    private int day, month, year;
    String operatingUnit;
    List<MultipartBody.Part> partList = new ArrayList<>();
    CompanyBankNameApater comapyBankNameAdapter;
    EditText etDepositorBranch;
    TextView tvCustomerCode, tvCustomerName, tvEntryDate;
    LinearLayout liCustomer;
    private static final int CAMERA_REQUEST = 1888;
    private String pictureImagePath;
    Uri photoURI;
    private static final int REQUEST_CODE = 6384;
    private ArrayList<Uri> cameraarrayList;
    ViewPager viewPager;
    String filePathProfile;
    private Button btnSend;
    ArrayList<String> filepathList = new ArrayList<String>();
    ProgressDialog progressDialog;
    LinearLayout liCamera, liGallery;
    DatePickerDialog picker;
    private String depositSlip, depositAmount, companybankname, conmpanyBankAcoount, depoistdate, depositorBank, bankBranch, customerCode, customnerName, entryDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_entry);
        mContext = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        requestStoragePermission();
        initialization();
        try {
            getebsBankAcInfo();

        } catch (Exception e) {

        }

        // getBankAccountInfo("Bearer "+SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION));
        mcalendar = Calendar.getInstance();
        etDate = (EditText) findViewById(R.id.calender);
        day = mcalendar.get(Calendar.DAY_OF_MONTH);
        year = mcalendar.get(Calendar.YEAR);
        month = mcalendar.get(Calendar.MONTH);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDialog();
            }
        });
        getDepositorBank();
        getCustomerInfo();
        liCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                String imageFileName = timeStamp + ".jpg";
                File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
                File file = new File(pictureImagePath);
                viewPager.setVisibility(View.VISIBLE);
                photoURI = Uri.fromFile(file);
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

            }
        });
        liGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilePicker();
            }
        });

    }

    private void openFilePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.e("dont click", "click");
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            try {
                cameraarrayList = new ArrayList<>();
                cameraarrayList.add(photoURI);
                viewPager.setVisibility(View.VISIBLE);
                MyCustomPagerAdapter myCustomPagerAdapter = new MyCustomPagerAdapter(mContext, cameraarrayList);
                viewPager.setAdapter(myCustomPagerAdapter);

                Log.e("click", String.valueOf(photoURI));
                Log.e("CAMERA_REQUEST", String.valueOf(cameraarrayList.size()));
            } catch (Exception e) {
                Log.e("error", "File select error", e);
            }

        } else if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            try {
                if (data.getData() != null) {
                    ArrayList<Uri> singleArraylist = new ArrayList<>();
                    Uri selectedImage = data.getData();
                    singleArraylist.add(selectedImage);
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String photoPath = cursor.getString(columnIndex);
                    cursor.close();
                    filePathProfile = photoPath;

                    viewPager.setVisibility(View.VISIBLE);
                    MyCustomPagerAdapter customPagerAdapter = new MyCustomPagerAdapter(mContext, singleArraylist);
                    viewPager.setAdapter(customPagerAdapter);
                    /*viewPager.setCurrentItem();*/
                    filepathList.add(photoPath);

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String photoPath = cursor.getString(columnIndex);
                            Log.e("path", photoPath + "");
                            filepathList.add(photoPath);
                        }
                        viewPager.setVisibility(View.VISIBLE);
                        MyCustomPagerAdapter customPagerAdapter = new MyCustomPagerAdapter(mContext, mArrayUri);
                        viewPager.setAdapter(customPagerAdapter);
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
                }
            } catch (Exception e) {

            }
        }
    }

    public void initialization() {
        progressDialog = new ProgressDialog(mContext);
        camera = findViewById(R.id.camera);
        liCamera = findViewById(R.id.liCamera);
        liGallery = findViewById(R.id.liGallery);
        gallery = findViewById(R.id.gallery);
        sprOperatingUnit = findViewById(R.id.sprOperatingUnit);
        spnrComapnyBankName = findViewById(R.id.sprCompanyBankName);
        sprBankAccountName = findViewById(R.id.sprBankAccountName);
        spnrDepositorBankAccoount = findViewById(R.id.spnrDepositorBankAccoount);
        edtCalender = findViewById(R.id.calender);
        edtamount = findViewById(R.id.edtamount);
        etDepositorBranch = findViewById(R.id.etDepositorBranch);
        tvCustomerCode = findViewById(R.id.tvCustomerCode);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvEntryDate = findViewById(R.id.tvCustomerDate);
        liCustomer = findViewById(R.id.liCustomer);
        viewPager = findViewById(R.id.viewPager);
        viewPager.setVisibility(View.GONE);
        btnSend = findViewById(R.id.btnSend);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        Log.e("formatted string: ", sdf.format(c.getTime()));
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InterNetConnection.checkConn(mContext)) {
                    if (inputValidation()) {
                        bankBranch = etDepositorBranch.getText().toString();
                        depositAmount = edtamount.getText().toString();
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
                        entryDate = sdf.format(c.getTime());
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("depositSlip", "");
                        jsonObject.addProperty("amount", depositAmount);
                        jsonObject.addProperty("ou", operatingUnit);
                        if (companybankname.equals(null)) {

                            Toast.makeText(CollectionEntryActivity.this, "Please insert Company bank Name", Toast.LENGTH_SHORT).show();
                        } else {
                            jsonObject.addProperty("bankName", companybankname);
                        }
                        if (conmpanyBankAcoount.equals(null)) {
                            Toast.makeText(CollectionEntryActivity.this, "Please insert Company Account Name", Toast.LENGTH_SHORT).show();
                        } else {

                            jsonObject.addProperty("bankAccount", conmpanyBankAcoount);
                        }
                        jsonObject.addProperty("depositDate", depoistdate);
                        if (depositorBank.equals(null)) {
                            Toast.makeText(CollectionEntryActivity.this, "Please insert Depositor Bank Name", Toast.LENGTH_SHORT).show();

                        } else {
                            jsonObject.addProperty("depositorBank", depositorBank);
                        }
                        if (bankBranch.equals(null)) {
                            Toast.makeText(CollectionEntryActivity.this, "Please insert Branch Name", Toast.LENGTH_SHORT).show();

                        } else {
                            jsonObject.addProperty("depositorBankBranch", bankBranch);
                        }

                        jsonObject.addProperty("customerCode", customerCode);
                        jsonObject.addProperty("customerName", customnerName);
                        jsonObject.addProperty("entryDate", entryDate);
                        jsonObject.addProperty("remarks", "Test");
                        jsonObject.addProperty("createdDate", entryDate);
                        jsonObject.addProperty("createdBy", SharedPreference.getStringValue(mContext, SharedPreference.USER_ID));
                        jsonObject.addProperty("modifiedDate", "");
                        jsonObject.addProperty("modifiedBy", "");
                        try {
                            sendData(jsonObject);
                        } catch (Exception e) {

                        }

                    }
                } else {
                    Toast.makeText(mContext, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void DateDialog() {

        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(CollectionEntryActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                etDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            }
        }, year, month, day);
        picker.show();
    }


    private void getebsBankAcInfo() {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiRequests.URL).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<List<BankAccountInfo>> getModelName = apiInterface.getCompanyBank("Bearer " + SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION));
        getModelName.enqueue(new Callback<List<BankAccountInfo>>() {
            @Override
            public void onResponse(Call<List<BankAccountInfo>> call, Response<List<BankAccountInfo>> response) {
                final List<BankAccountInfo> modelName = response.body();
                BankInfoAdapter ouAdapter = new BankInfoAdapter(mContext, modelName);
                sprOperatingUnit.setAdapter(ouAdapter);
               /* comapyBankNameAdapter = new CompanyBankNameApater(mContext, modelName);
                spnrComapnyBankName.setAdapter(comapyBankNameAdapter);*/
                final BankAccountName bankAccountName = new BankAccountName(mContext, modelName);
                sprBankAccountName.setAdapter(bankAccountName);
                sprOperatingUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        operatingUnit = modelName.get(i).getOu();
                        int orgraId = modelName.get(i).getOrganizationId();
                        try {
                            getCompanyBankname(orgraId);

                        } catch (Exception e) {

                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                sprBankAccountName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        conmpanyBankAcoount = modelName.get(i).getBankAccountName();
                        Log.e("modelName", conmpanyBankAcoount);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<BankAccountInfo>> call, Throwable t) {
                Log.e("failur", t.getMessage());
                progressDialog.dismiss();

            }
        });
    }

    private void getCompanyBankname(int orgraId) {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiRequests.URL).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<List<BankAccountInfo>> getModelName = apiInterface.getSpinnerData("Bearer " + SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION), orgraId);
        getModelName.enqueue(new Callback<List<BankAccountInfo>>() {
            @Override
            public void onResponse(Call<List<BankAccountInfo>> call, Response<List<BankAccountInfo>> response) {
                final List<BankAccountInfo> modelName = response.body();

                comapyBankNameAdapter = new CompanyBankNameApater(mContext, modelName);
                spnrComapnyBankName.setAdapter(comapyBankNameAdapter);

                spnrComapnyBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        companybankname = modelName.get(i).getBankName();
                        Log.e("modelName", companybankname);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<BankAccountInfo>> call, Throwable t) {
                Log.e("failur", t.getMessage());
                progressDialog.dismiss();

            }
        });
    }

    private void getDepositorBank() {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiRequests.URL).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<List<DepositorBank>> getModelName = apiInterface.getDepositorBank("Bearer " + SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION));
        getModelName.enqueue(new Callback<List<DepositorBank>>() {
            @Override
            public void onResponse(Call<List<DepositorBank>> call, Response<List<DepositorBank>> response) {

                Log.e("response modelname", response.toString());
                final List<DepositorBank> depositorBanks = response.body();
                DepositorBankAdapter depositorBankAdapter = new DepositorBankAdapter(mContext, depositorBanks);
                spnrDepositorBankAccoount.setAdapter(depositorBankAdapter);

                spnrDepositorBankAccoount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        depositorBank = depositorBanks.get(i).getDescription();
                        Log.e("depostitebank", depositorBank);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<DepositorBank>> call, Throwable t) {
                Log.e("failur", t.getMessage());
                progressDialog.dismiss();

            }
        });
    }

    private void getCustomerInfo() {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiRequests.URL).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build();
        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<List<CustomerInfo>> getModelName = apiInterface.getCustomerInfo("Bearer " + SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION));
        getModelName.enqueue(new Callback<List<CustomerInfo>>() {
            @Override
            public void onResponse(Call<List<CustomerInfo>> call, Response<List<CustomerInfo>> response) {

                Log.e("response modelname", response.toString());
                final List<CustomerInfo> depositorBanks = response.body();
                tvCustomerCode.setText("Customer Code: " + depositorBanks.get(0).getPartyId());
                customerCode = String.valueOf(depositorBanks.get(0).getPartyId());
                tvCustomerName.setText("Customer Name: " + depositorBanks.get(0).getCustomerName());
                customnerName = depositorBanks.get(0).getCustomerName();
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                entryDate = date;
                tvEntryDate.setText("Entry Date: " + date);
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<CustomerInfo>> call, Throwable t) {
                Log.e("failur", t.getMessage());
                progressDialog.dismiss();

            }
        });
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this).withPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {

                }
                if (report.isAnyPermissionPermanentlyDenied()) {
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                }).onSameThread().check();
    }


    private void sendData(JsonObject jsonObject) {
        progressDialog.setTitle("Loading ..... please wait");
        progressDialog.show();
        Log.e("inside", "ufff");
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(ApiRequests.URL).
                addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();

        ApiRequests apiInterface = retrofit.create(ApiRequests.class);
        Call<AppsCollection> callSpecificaiotn = apiInterface.appcollecton("Bearer " + SharedPreference.getStringValue(mContext, SharedPreference.AUTHRIZATION), jsonObject);
        callSpecificaiotn.enqueue(new Callback<AppsCollection>() {
            @Override
            public void onResponse(Call<AppsCollection> call, Response<AppsCollection> response) {
                Log.e("response of features", response.toString());
                AppsCollection appsCollection = response.body();
                Toast.makeText(mContext, "Transaction Id" + appsCollection.getId(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<AppsCollection> call, Throwable t) {
                Log.e("failur", "failur submit feedback");
                progressDialog.dismiss();
                Log.e("error", t.getMessage());

            }
        });
    }

    private boolean inputValidation() {
        if (edtamount.getText() == null || edtamount.getText().toString().isEmpty()) {
            UserDialog.showUserAlert(mContext,getString(R.string.amanot_alert),"কালেকশন এন্ট্রি");
            return false;
        } else if (etDate.getText().toString().trim().isEmpty()) {
            UserDialog.showUserAlert(mContext, getString(R.string.tarikh_alert),"কালেকশন এন্ট্রি");
            return false;
        } else if (etDepositorBranch.getText().toString().trim().isEmpty()) {
            UserDialog.showUserAlert(mContext, getString(R.string.branchname_alert),"কালেকশন এন্ট্রি");
            return false;
        }
        return true;
    }
}



