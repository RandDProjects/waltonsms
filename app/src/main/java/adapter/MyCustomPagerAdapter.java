package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.walton.productregistration.R;

import java.io.IOException;
import java.util.ArrayList;

public class MyCustomPagerAdapter extends PagerAdapter {
    Context context;
    private ArrayList<Uri> cameraarrayList;
    LayoutInflater layoutInflater;


    public MyCustomPagerAdapter(Context context, ArrayList<Uri> cameraarrayList1) {
        this.context = context;
        cameraarrayList = cameraarrayList1;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return cameraarrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item, container, false);
        ImageView imageView =  itemView.findViewById(R.id.imageView);
        Bitmap bitmap = null;
        try {
            Log.e("sixeee", String.valueOf(cameraarrayList.size()));
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), cameraarrayList.get(position));
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageView.setImageBitmap(bitmap);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
