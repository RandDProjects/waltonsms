package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.model.BankAccountInfo;
import com.walton.productregistration.R;

import java.util.List;

public class BankAccountName  extends BaseAdapter implements SpinnerAdapter {
    Context context;
    List<BankAccountInfo> bankAccountInfoList;

    public   BankAccountName(Context mContext, List<BankAccountInfo> bankAccountInfos) {
        context = mContext;
        bankAccountInfoList = bankAccountInfos;
    }

    @Override
    public int getCount() {
        return bankAccountInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return bankAccountInfoList.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view =  View.inflate(context, R.layout.company_main, null);
        TextView textView = view.findViewById(R.id.main);
        textView.setText(bankAccountInfoList.get(i).getBankAccountName());
        return textView;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view;
        view =  View.inflate(context, R.layout.company_dropdown, null);
        final TextView textView = (TextView) view.findViewById(R.id.dropdown);
        textView.setText(bankAccountInfoList.get(position).getBankAccountName());

        return view;
    }
}

